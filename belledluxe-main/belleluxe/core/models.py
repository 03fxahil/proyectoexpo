from audioop import reverse
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

TipoGenero=(("Hombre","Hombre"),("Mujer","Mujer"))

class campaign(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=30, default='generico')
    year=models.DateField(auto_now_add=True)
    def __str__(self):
        return self.name



class Category(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=25, default='generic category')
    
    class Meta:
        verbose_name_plural = 'categories'
    def get_absolute_url(self):
        return reverse('core:category', args=[str(self.name)])
    def __str__(self):
        return self.name



class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name_first=models.CharField(max_length=35)
    last_name_pat=models.CharField(max_length=25)
    last_name_mat=models.CharField(max_length=25, blank=True, null=True)
    phone=models.CharField(max_length=15)
    cp=models.CharField(max_length=5)
    streat=models.CharField(max_length=25)
    number_home=models.CharField(max_length=5)
    def __str__(self):
        return self.name_first


        

#Agregar un max_length al stock
class Product(models.Model):
    lasteditor = models.ForeignKey(User, on_delete=models.CASCADE)
    name=models.CharField(max_length=35)   
    campaign=models.ForeignKey(campaign, on_delete=models.CASCADE)
    gender=models.CharField(max_length=10, choices=TipoGenero)
    category=models.ForeignKey(Category, on_delete=models.CASCADE)
    description=models.CharField(max_length=256)
    size=models.CharField(max_length=4)
    price=models.FloatField(default=0.0)
    status=models.BooleanField(default=False)
    stock=models.IntegerField(max_length=3,default=0)
    slug=models.SlugField(max_length=36)
    imagen1 = models.ImageField(upload_to='media/', null=True) 

    
    class Meta:
        verbose_name_plural = 'Products'
        ordering = ('-status',)

    def get_absolute_url(self):
        return reverse('core:product', args=[self.slug])
    
    def __str__(self):
        return self.name
#Hacer CRUD
class ItemPedido(models.Model):
    pedido = models.ForeignKey('Pedido', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    cantidad = models.PositiveIntegerField()
    codigo_producto = models.CharField(max_length=20, unique=True) 

#hacer CRUD
class Pedido(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    productos = models.ManyToManyField(Product, through=ItemPedido)
    fecha = models.DateTimeField(auto_now_add=True)
    codigo_pedido = models.CharField(max_length=20, unique=True)