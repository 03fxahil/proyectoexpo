from django.contrib import admin
from django.urls import path, include
from home import views


app_name="home"
urlpatterns=[
    path('', views.inicio.as_view(), name="inicio"),
    path('nosotros/', views.nosotros.as_view(), name="nosotros"),
    path('blog/', views.blog.as_view(), name="blog"),
    path('servicios/', views.servicios.as_view(), name="servicios"),
    path('politicas/', views.politicas.as_view(), name="politicas"),
    path('perfil/<int:pk>/', views.perfil.as_view(), name="perfil"),

# Accesos
    path('registro/', views.registrar.as_view(), name='registro'),
    path('acceso/', views.Acceso.as_view(), name='acceso'),
    path('cierre/',views.cierre, name='cierre'),
    path('perfil/actualiar/<int:pk>/', views.UpdateProfile.as_view(), name="actualizarPerfil"),

]



