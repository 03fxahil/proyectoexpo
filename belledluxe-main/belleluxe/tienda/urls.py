
from django.contrib import admin
from django.urls import path
from tienda import views


#  Compras 
app_name= "tienda"
urlpatterns=[
    path('', views.tienda.as_view(), name="tienda"),
    path('hombre/', views.hombre.as_view(), name="hombre"),
    path('mujer/', views.mujer.as_view(), name="mujer"),
    path('producto/<int:pk>/', views.producto.as_view(), name='producto'),
    path('categoria/<int:pk>/', views.Pcategoria.as_view(), name='Pcategoria'),
    path('busqueda/', views.busqueda.as_view(), name='busqueda'),
]